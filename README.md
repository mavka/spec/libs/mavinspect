MAVLink Inspector Protobuf Specification 
========================================

Contains [Protobuf](https://protobuf.dev/) specification for
[MAVLink Inspector](https://gitlab.com/mavka/mavinspect).

Depends on MAVLink Protobuf [message definitions v1.0](https://gitlab.com/mavka/spec/protocols/mavlink/message-definitions-v1) 
specification. These messages are included as [`message-definitions-v1`](message-definitions-v1) Git submodule.
